global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

%define EXIT_SYSCALL 60
%define WRITE_SYSCALL 1

%define NEWLINE_SYMBOL `\n`
%define MINUS_SYMBOL `-`
%define TAB_SYMBOL `\t`
%define SPACE_SYMBOL ` `
%define PLUS_SYMBOL `+`

%define STDOUT 1
%define STDIN 0

section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_SYSCALL ; exit syscall
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax ; обнуляем счётчик

    .loop:
        cmp byte[rdi+rax], 0
        je .end ; если это нуль-терминатор, то строка окончена

        inc rax ; прибавление к количеству

        jmp .loop

    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi ; передаем указатель на строку в параметр syscall
    
    mov rdx, rax ; записываем количество байт для вывода (количество символов)
    
    mov rax, WRITE_SYSCALL ; syscall for write
    mov rdi, STDOUT ; дескриптор (stdout)

    syscall

    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEWLINE_SYMBOL ; загрузка символа \n
; Принимает код символа и выводит его в stdout
print_char:
    push rdi ; сохраняем код символа для указания на него

    mov rax, WRITE_SYSCALL ; номер syscall (write)
    mov rdi, STDOUT ; stdout
    mov rsi, rsp ; берем указатель на код символа для печати
    mov rdx, 1 ; количество байт (1 символ)

    syscall

    pop rdi ; очистка стека
    
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    ; проверка на отрицательность
    test rdi, rdi
    jge print_uint; если больше либо равно 0

    neg rdi
    push rdi
    ; печать символа минуса
    mov rdi, MINUS_SYMBOL
    call print_char

    pop rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r10, rsp ; сохранение указателя стека
    mov rax, rdi ; перемещаем делимое в рабочий регистр
    mov r11, 10 ; система счисления - 10

    dec rsp ; сдвигаем stack pointer
    mov byte[rsp], 0x0 ; добавляем в конец нуль-терминатор

    .loop:
        xor rdx, rdx ; очистка регистра
        div r11 ; производим деление на 10

        add rdx, '0' ; переводим в ASCII-код (Не магическая константа, но зачем прибавляем ноль умолчим)

        dec rsp ; сдвигаем stack pointer
        mov [rsp], dl ; сохраняем цифру

        ; проверка на конец деления
        test rax, rax
        jz .printing

        jmp .loop
    .printing:
        mov rdi, rsp
        call print_string

        mov rsp, r10 ; восстановление указателя стека
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    cmp rdi, rsi ; сравнение двух указателей
    je .equals ; если они равны, то строки равны

    .loop:
        ; загрузка символов из строчек
        mov r10b, [rdi]
        mov r11b, [rsi]

        cmp r10b, r11b ; сравнение двух символов

        jne .not_equals ; если символы не совпадают

        ; проверка на конец строки
        test r10b, r10b
        jz .equals

        ; сдвиг на следующие символы
        inc rdi
        inc rsi
        jmp .loop

    ; строки равны
    .equals:
        mov rax, 1
        ret
    ; строки не равны
    .not_equals:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax ; read syscall
    xor rdi, rdi ; stdin
    mov rdx, 1
    push rax ; подготовка к записи в стэк
    mov rsi, rsp ; указание на запись в стэк

    syscall

    pop rax ; извлечение значения из стэка

    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    mov r12, rdi ; сохранение указателя
    mov r13, rsi

    .skip_space_symbols:
        call read_char

        cmp al, SPACE_SYMBOL
        je .skip_space_symbols

        cmp al, TAB_SYMBOL
        je .skip_space_symbols

        cmp al, NEWLINE_SYMBOL
        je .skip_space_symbols
    xor rcx, rcx
    .loop:
        cmp al, 0x0
        je .end_of_stream

        cmp al, SPACE_SYMBOL
        je .end_of_stream

        cmp al, TAB_SYMBOL
        je .end_of_stream

        cmp al, NEWLINE_SYMBOL
        je .end_of_stream

        cmp rcx, r13
        jg .fault

        mov byte[r12+rcx], al
        inc rcx

        push rcx
        call read_char
        pop rcx

        jmp .loop

    .fault:
        xor rax, rax

        pop r13
        pop r12
        ret

    .end_of_stream:
        mov byte[r12+rcx], 0x0
        mov rax, r12
        mov rdx, rcx

        pop r13
        pop r12
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor r10, r10
    .loop:
        mov r10b, byte[rdi+rdx] ; чтение символа

        ; проверка на вхождение символа в диапазон цифр
        cmp r10b, '0'
        jl .result

        cmp r10b, '9'
        jg .result

        ; сдвиг символа
        imul rax, 10

        ; перевод из ASCII кода в цифру
        sub r10b, '0'

        ; прибавление цифры к числу
        add rax, r10

        ; сдвиг указателя
        inc rdx
        jmp .loop
    .result:
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov r10b, [rdi]

    cmp r10b, MINUS_SYMBOL ; проверка на минус
    je .print_neg_number

    cmp r10b, PLUS_SYMBOL ; проверка на плюс
    je .print_plus_number

    ; проверка на вхождение символа в диапазон цифр
    cmp r10b, '0'
    jl .fault

    cmp r10b, '9'
    jg .fault

    jmp .print_number

    .print_plus_number:
        inc rdi ; удаление плюса
        call parse_uint
        inc rdx ; учет знака
        ret
    .print_number:
        call parse_uint
        ret
    .print_neg_number:
        inc rdi ; удаление минуса
        call parse_uint
        inc rdx ; учет знака
        neg rax
        ret
    .fault:
        xor rdx, rdx
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:

    ; подсчёт длины строки
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi

    cmp rax, rdx
    jge .fault

    xor rax, rax ; счётчик
    .loop:
        cmp byte[rdi+rax], 0
        je .end

        mov r9b, byte[rdi+rax] ; temp
        mov byte[rsi+rax], r9b

        inc rax
        jmp .loop

    .end:
        mov byte[rsi+rax], 0
        ret

    .fault:
        xor rax, rax
        ret